import React from 'react';

export default function covid() {
    return (
        <div >
            <div className="bannerMain">
                <div className="header">
                    <div className="img2-banner"></div>

                    <span className="fl">
                        <div className="Title-banner">Safety+</div>
                        <div className="subtext-banner">
                            Opt to Travel Safe with redBus
                    <a className="know-more-anchor" href="/SafetyPlus">&nbsp;Know more</a>
                        </div>
                    </span>
                    <span className="fr">
                    </span>

                </div>
                <div className="subHeader-banner">
                    <span className="img3-banner"></span>
                    <span className="bannertext1">Look for buses with </span>
                    <span className="img4-banner"></span>
                    <span className="text">tag while booking your journey, </span>
                </div>


                <div className="info-banner">
                    <div className="titl-banner"> Sanitized Bus </div>
                    <div className="value-banner"> </div>
                    <div className="text-banner">All Safety+ buses are sanitized and disinfected before and after every trip. </div>
                </div>
                <div className="info-banner2">
                    <div className="titl-banner"> Mandatory masks </div>
                    <div className="value-banner"> </div>
                    <div className="text-banner">Proper masks are mandatory for all passengers and bus staff.</div>
                </div>
                <div className="info-banner2">
                    <div className="titl-banner"> Thermal Screening </div>
                    <div className="value-banner"> </div>
                    <div className="text-banner">All passengers will undergo thermal screening. Temperature checks for bus drivers and
                service staff are done before every trip. </div>
                </div>


            </div>
        </div>
    )
}