import React from 'react'

export default function Awards() {
    return (
        <div style={container}>
            <h2 style={{ fontSize: '2.4rem' }}>OUR GLOBAL PRESENCE</h2>
            <div style={cards}>
                <div style={card}>
                    <h4>COLOMBIA</h4>
                    <h4>MALAYSIA</h4>
                </div>

                <div style={card}>
                    <h4>INDIA</h4>
                    <h4>PERU</h4>
                </div>

                <div style={card}>
                    <h4>INDONESIA</h4>
                    <h4>SINGAPORE</h4>
                </div>
            </div>
        </div>
    )
}

const container = {
    marginTop: '0.2rem',
    width: '100vw',
    height: 'max-content',
    margin: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f2cda0',
    padding: '1rem',
    // border: '0.1rem solid #000'
}

const cards = {
    display: 'flex',

}

const card = {
    width: '18rem',
    height: '15rem',
    padding: '1rem',
    // border: '0.1rem solid #444', 
    fontSize: '1rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
}
