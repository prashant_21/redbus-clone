import React from 'react'

export default function Footer() {
    return (
        <div style={footer}>
            <div style={c1}>
                <h3>About RedBus</h3>
                <a style={anchor} href='#'>About Us</a>
                <a style={anchor} href='#'>Contact Us</a>
                <a style={anchor} href='#'>Mobile Version</a>
                <a style={anchor} href='#'>redBus on Mobile</a>
                <a style={anchor} href='#'>Sitemap</a>
                <a style={anchor} href='#'>Offers</a>
                <a style={anchor} href='#'>Careers</a>
                <a style={anchor} href='#'>Values</a>
            </div>

            <div style={c1}>
                <h3>Info</h3>
                <a style={anchor} href='#'>T &amp; C</a>
                <a style={anchor} href='#'>Privacy Policy</a>
                <a style={anchor} href='#'>FAQ</a>
                <a style={anchor} href='#'>Blog</a>
                <a style={anchor} href='#'>Agent Registration</a>
                <a style={anchor} href='#'>Insurance Partner</a>
                <a style={anchor} href='#'>User Agreement</a>
            </div>

            <div style={c1}>
                <h3>Our Partners</h3>
                <a style={anchor} href='#'>Goibibo</a>
                <a style={anchor} href='#'>Makemytrip</a>
            </div>

            <div style={c3}>
                <img style={{width: '4rem', height: '3rem'}} src="https://s3.rdbuz.com/web/images/home/sgp/r_logo.png" />
                <p style={{color: '#fff'}}>redBus is the world's largest online bus ticket booking service trusted by over 18 million happy customers
                globally. redBus offers bus ticket booking through its website,iOS and Android mobile apps for all major
            routes.</p>
            </div>
        </div>
    )
}

const footer = {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: '#010812',
    fontSize: '1rem',
    padding: '1rem',
    width: '100vw',
}

const c1 = {
    display: 'flex',
    flexDirection: 'column',
    margin: '1rem 2.5rem',
    color: '#fff',
}

const anchor = {
    textDecoration: 'none',
    color: '#fff',
}

const c3 = {
    margin: '1.4rem',
    marginLeft: '4rem',
    width: '18rem',
}