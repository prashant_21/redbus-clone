import React from 'react';
import './MainHeader.css';
import redBusLogo  from '../../../Images/redBusLogo.png'; 


function MainHeader() {
    return (
        <div className="MainHeaderSection">
            <nav className="navbar navbar-expand-lg" style={{backgroundColor:'#d84f57',marginTop:'0px',padding:'0px'}}>
                <div className="container-fluid" style={{marginLeft:'200px',marginRight:'200px',marginTop:'0px',padding:'0px'}}>
                    <a className="navbar-brand" href="#" style={{margin:'0'}}>
                        <img src={redBusLogo} style={{width:'60px' ,height:'50px',outline:'none'}}/>
                    </a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent" style={{margin:'0' }}>
                        <ul className="navbar-nav me-auto " >
                            <li className="nav-item" style={{ }}> <a style={{fontWeight:"700"}} className="nav-link active" aria-current="page" href="#">BUS TICKETS</a></li>
                            <li className="nav-item"> 
                                <a className="nav-link" href="#" style={{display:'flex'}}>
                                    <div>rPool</div>&nbsp;
                                <div style={{marginTop:'-2px',fontSize:'10px',letterSpacing:'.2px'}}>New</div>
                                </a>
                            </li>
                            <li className="nav-item"> <a className="nav-link" href="#">BUS HIRE</a> </li>
                        </ul>
                        <div className="d-flex">
                            <li className="nav-item"> <a className="nav-link" href="#">Help</a> </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link " 
                                href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> Manage Booking  
                                <i class="fal fa-chevron-down fa-md" style={{marginLeft:'5px',MarginTop:'8px'}}></i>
                                </a>
                             
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li ><a className="dropdown-item disabled" href="#">Bus Tickets</a></li>
                                <li><a className="dropdown-item" href="#">Cancel</a></li>
                                <li><a className="dropdown-item" href="#">Change Travel Date</a></li>
                                <li><a className="dropdown-item" href="#">Show My Tickets</a></li>
                                <li><a className="dropdown-item" href="#">Email/SMS</a></li>
                            </ul>
                            </li>
                            <li className="nav-item dropdown" style={{ margin:'0px',padding:'0px'}}>  
                                <a className="nav-link " style={{ margin:'0px',padding:'0px'}}
                                href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="far fa-user-circle fa-3x "   ></i>
                                    {/* <i class="fal fa-chevron-down fa-md" style={{marginLeft:'5px'  }}></i> */}
                                 </a>

                                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    < li><a className="dropdown-item" href="#">Sign In/Sign up</a></li>
                                </ul>
                            </li>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default MainHeader
