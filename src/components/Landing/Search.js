import React, { Component } from 'react';
import Date from '../Date';
import banner from '../../image/bus.png';

export class Search extends Component {
    render() {
        return (
            <div style={searchContainer}>
                <div style={searchStyle}>
                    <input style={{padding: '0.2rem 0.6rem', border: '0.2rem solid red'}} placeholder='From'></input>
                    <input style={{padding: '0.2rem 0.6rem', border: '0.2rem solid red'}} placeholder='To'></input>
                    <Date />
                    <button style={btnStyle}>Search Buses</button>
                </div>
            </div>
        )
    }
}

const searchContainer = {
    background: `url(${banner})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    width: '100vw',
    height: '60vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
}

const searchStyle = {
    display: 'flex',
    width: 'max-content',
    padding: '1rem',
}

const btnStyle = {
    backgroundColor: 'red',
    color: '#fff',
    padding: '0 1rem',
    border: 'none',
}

export default Search
