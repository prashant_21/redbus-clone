import React from 'react'

export default function Promise() {
    return (
        <div style={container}>
            <div >
                <div style={mainHeader}>
                    <img src="//s1.rdbuz.com/web/images/home/promise.png" height="100" />
                    <h2 style={{marginTop: '1rem'}}> We promise to deliver</h2>
                </div>
            </div>

            <div style={promises}>
                <div style={cards}>
                    <img src="//s3.rdbuz.com/Images/safety/srp/safety.svg" height="90" />
                    <h3>SAFETY+</h3>
                    <h5>With Safety+ we have brought in a set of measures like Sanitized buses, mandatory masks etc. to ensure
                you travel safely.</h5>
                </div>

                <div style={cards}>
                    <img src="//s1.rdbuz.com/web/images/home/customer_care.png" height="100" />
                    <h3>SUPERIOR CUSTOMER SERVICE</h3>
                    <h5>We put our experience and relationships to good use and are available to solve your travel issues.</h5>
                </div>

                <div style={cards}>
                    <img src="//s1.rdbuz.com/web/images/home/lowest_Fare.png" height="90" />
                    <h3>LOWEST PRICES</h3>
                    <h5>We always give you the lowest price with the best partner offers.</h5>
                </div>

                <div style={cards}>
                    <img src="//s2.rdbuz.com/web/images/home/benefits.png" height="90" />
                    <h3>UNMATCHED BENEFITS</h3>
                    <h5>We take care of your travel beyond ticketing by providing you with innovative and unique benefits.</h5>
                </div>
            </div>
        </div>
    )
}

const container = {
    marginTop: '2rem',
    width: '100vw',
    height: 'max-content',
    margin: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f2cda0',
    padding: '2rem',
}

const mainHeader = {
    fontWeight: '600',
    textTransform: 'uppercase',
    fontSize: '30px',
    lineHeight: '1.1em',
    color: '#fff',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
}

const promises = {
    marginTop: '2rem',
    display: 'flex',
    textAlign: 'center',
}

const cards = {
    width: '18rem',
    height: '20rem',
    padding: '1rem',
    marginTop: '1rem',
    border: '0.1rem solid #f4f4f4', 
    fontSize: '1rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#f2ab6d',
}