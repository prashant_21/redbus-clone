import React from 'react'

export default function Awards() {
    return (
        <div style={container}>
            <h2 style={{ fontSize: '2.4rem' }}>AWARDS &amp; RECOGNITION</h2>
            <div style={cards}>
                <div style={card}>
                    <img src="//s2.rdbuz.com/web/images/home/awards/Business_Standard1.png" />
                    <h4>Most Innovative Company</h4>
                </div>

                <div style={card}>
                    <img src="//s1.rdbuz.com/web/images/home/awards/Brand_Trust_Report.png" />
                    <h4>Most Trusted Brand</h4>
                </div>

                <div style={card}>
                    <img src="//s3.rdbuz.com/web/images/home/awards/Eye_for_Travel1.png" />
                    <h4>Mobile Innovation Award</h4>
                </div>
            </div>
        </div>
    )
}

const container = {
    marginTop: '2rem',
    width: '100vw',
    height: 'max-content',
    margin: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#f2cda0',
    padding: '2rem',
    boxShadow: '0 0.2rem 0.5rem #f2ab6d'

}

const cards = {
    display: 'flex',

}

const card = {
    width: '18rem',
    height: '20rem',
    padding: '1rem',
    margin: '1rem',
    // border: '0.1rem solid #444', 
    fontSize: '1rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
}
