import React from 'react'

export default function Awards() {
    return (
        <div style={container}>
            <h2 style={{ fontSize: '2.4rem' }}>The numbers are growing!</h2>
            <div style={cards}>
                <div style={card}>
                    <h2>CUSTOMERS</h2>
                    <h1 style={{ fontSize: '4rem' }}>23 M</h1>
                    <h5 style={{ fontSize: '1rem', textAlign: 'center' }}>RedBus is trusted by over 23 million happy customers globally</h5>
                </div>

                <div style={card}>
                    <h2>OPERATORS</h2>
                    <h1 style={{ fontSize: '4rem' }}>2600</h1>
                    <h5 style={{ fontSize: '1rem', textAlign: 'center' }}>Network of over 2600 bus operators worldwide</h5>
                </div>

                <div style={card}>
                    <h2>BUS TICKETS</h2>
                    <h1 style={{ fontSize: '3rem' }}>180+ M</h1>
                    <h5 style={{ fontSize: '1rem', textAlign: 'center' }}>Over 180 million trips booked using redBus</h5>
                </div>
            </div>
        </div>
    )
}

const container = {
    marginTop: '2rem',
    width: '100vw',
    height: 'max-content',
    margin: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#f2cda0',
    padding: '3rem',
    boxShadow: '0 0.2rem 0.5rem #f2ab6d',
    textTransform: 'uppercase',
}

const cards = {
    display: 'flex',
    marginTop: '1rem',
    alignItems: 'baseline',
}

const card = {
    width: '16rem',
    height: '15rem',
    padding: '1rem',
    margin: '1rem 1rem 1rem 1rem',
    // border: '0.1rem solid #444', 
    fontSize: '1rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
}
