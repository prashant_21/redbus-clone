import './App.css';
import MainHeader from './components/Landing/MainHeader/MainHeader';

import Search from './components/Landing/Search';
import Covid from './components/Landing/Covid';
import Promise from './components/Landing/Promise';
import Awards from './components/Landing/Awards';
import Presence from './components/Landing/Presence';
import Growing from './components/Landing/Growing';
import Footer from './components/Landing/Footer';

function App() {
  return (
    <div className="App">
      <MainHeader/>
      <Search />
      <Covid />
      <Promise />
      <Awards />
      <Presence />
      <Growing />
      <Footer />
    </div>
  );
}

export default App;
